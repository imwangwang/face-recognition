import os

from dataset.faceDataSet import FaceDataSet


class YaleFaceDataSet(FaceDataSet):

    def __init__(self, path, ext_list, n_classes):
        super().__init__(path, ext_list, n_classes)

    def process_label(self, img_path, labels):
        val = int(os.path.split(img_path)[1].split(".")[0].replace("subject", "")) - 1
        if val not in labels:
            self.number_labels+=1
#        print("img_path:%s, val:%d, self.number_labels:%d", img_path, val, self.number_labels)
        return val
