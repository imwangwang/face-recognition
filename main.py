#!/usr/bin/env python

"""Face recognition module using Tensorflow and Keras.

In this module we train a convolutional neural network
to be able to predict or recognize faces.
"""

__author__ = "Arnaldo Perez Castano"

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk

from skimage import io,color
from util.common import Common

from dataset.yaleFaceDataSet import YaleFaceDataSet
# Config
from face_detection.mtcnn_detector import MTCnnDetector
from face_recognition.model.convolutionalModel import ConvolutionalModel
from face_recognition.model.vggModel import VggModel
from util import constant

import tkinter as tk
from tkinter import filedialog
from tkinter import Button
from tkinter import ttk

from threading import Thread, Lock
import sys
import serial
import binascii
import queue

def receive(lser, root):
    global q
    global mutex

    connect_ack = bytearray([0x5A, 0x00, 0x00, 0x00])
    recog_ack = bytearray([0x5A, 0x01, 0x01, 0x00])
    while True:
        item = q.get()
        if item == "stop":
            continue
        elif item == "poll":
            mutex.acquire()
            dat = lser.read(4)
            if len(dat) != 0:
                print("Read %d Bytes from USB"%len(dat))
                print(binascii.hexlify(dat))
                if dat == connect_ack:
                    root.connect_label.config(background = 'green')
                    root.connect_label.config(text = 'Connected')
                elif dat == recog_ack:
                    root.recog_label.config(background = 'green')
                    root.recog_label.config(text = 'Matched! PASS!')
                else:
                    root.recog_label.config(background = 'red')
                    root.recog_label.config(text = 'NOT Matched!')
            mutex.release()
            q.put("poll")
        elif item == "quit":
            break

def dummy():
    return

def ask_for_predict_file(root):
    print("Please provide a location for a img file")
    try:
        file_path = filedialog.askopenfilename()
        root.file_path = file_path

        print(file_path)
        img = mpimg.imread(file_path)
        if img is not None:
            gray = img
            img = color.gray2rgb(gray)
            root.a.imshow(img)
            root.canvas.draw()
    except:
        root.file_path = None
        return

    return file_path

def train(root):
    global dataSet
    if root.cnn is not None:
        print("trainset is already ready to go")
        return
    print("training...")
    dataSet.get_data()
    root.cnn = ConvolutionalModel(dataSet)
    root.cnn.train(n_epochs=60)
    root.cnn.evaluate()
#    root.cnn.predict(np.expand_dims(dataSet.objects[1], axis=0))

def predict(root):
    global dataSet
    if root.file_path is None:
        print("target img is not valid")
        return
    if root.cnn is None:
        print("cnn is not ready")
        return
    image = io.imread(root.file_path, as_gray=False)
    image = np.array(image)
    image = Common.reshape_from_yalimg(image)
    result = root.cnn.predict(np.expand_dims(image, axis=0))
    i = 0
    j = 0
    val = 0
    while (i < dataSet.number_labels):
        if result[i] > val:
            val = result[i]
            j = i
        i += 1
    print("GOT:%d"%j)
    root.monthchoosen.current(j + 1)
    recog = bytearray([0xA5, 0x04, 0x00, 0x00])
    tagAddr = root.monthchoosen.get()
    tagSplit = tagAddr.split(',')
    if tagSplit[1] != 'INVALID':
        print(tagSplit[1], tagSplit[2])
        sendTxt = tagSplit[1] + ',' + tagSplit[2]
        recog += sendTxt.encode() + b'\0'
        print(binascii.hexlify(recog))
        mutex.acquire()
        ser.write(recog)
        mutex.release()
        q.put('poll')
        root.recog_label.config(background = 'blue')
        root.recog_label.config(text = 'Waiting Scan...')


def connect(root):
    global ser
    hello = bytearray([0xA5, 0x01, 0x00, 0x00])
    recog = bytearray([0xA5, 0x04, 0x00, 0x00])
    tagAddr = root.monthchoosen.get()
    tagSplit = tagAddr.split(',')
    if tagSplit[1] != 'INVALID':
        print(tagSplit[1], tagSplit[2])
        root.recog_label.config(background = 'blue')
        root.recog_label.config(text = 'Waiting Scan...')
        sendTxt = tagSplit[1] + ',' + tagSplit[2]
        recog += sendTxt.encode() + b'\0'
        print(binascii.hexlify(recog))
        mutex.acquire()
        ser.write(recog)
        mutex.release()
        q.put('poll')
    else:
        mutex.acquire()
        ser.write(hello)
        mutex.release()
        q.put('poll')
        print(tagSplit[1])


def test(root):
    if root.file_path is None:
        print("test image is not exist")
        return
    gray = io.imread(root.file_path, as_gray=False)
    image = color.gray2rgb(gray)
    print(type(image))
    print(image.shape[0])
    print(image.shape[1])
    print(image.shape[2])
    print(image.size)
    print(image[0][0])


class Root(tk.Tk):
    def __init__(self):
        super(Root, self).__init__()
        self.title("Python Tkinter Embeding Matplotlib")
        self.minsize(640, 400)
#        self.wm_iconbitmap("icon.ico")
        self.matplotCanvas()
        self.file_path = None
        self.cnn = None

    def mainloop(self):
        super().mainloop()

    def matplotCanvas(self):
        f = Figure(figsize=(5, 5), dpi=100)
        self.a = f.add_subplot(111)

        self.canvas = FigureCanvasTkAgg(f, self)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        toolbar = NavigationToolbar2Tk(self.canvas, self)
        toolbar.update()
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        self.button_file_open = Button(self, text="File Open", command = lambda: ask_for_predict_file(self), height=1, width=10, state="normal")
        self.button_file_open.place(x=12, y=2)

        self.button_train = Button(self, text="Train", command = lambda: train(self), height=1, width=10, state="normal")
        self.button_train.place(x=120, y=2)

        self.button_predict = Button(self, text="Predict", command = lambda: predict(self), height = 1, width = 10, state = "normal")
        self.button_predict.place(x=240, y=2)

        self.button_test = Button(self, text="Connect", command = lambda: connect(self), height = 1, width = 10, state = "normal")
        self.button_test.place(x=360, y=2)

        self.connect_label = ttk.Label(self, text = "Disconnect",
          background = 'red', foreground ="white",
          font = ("Times New Roman", 15))
        self.connect_label.place(x=480, y=2)

        self.recog_label = ttk.Label(self, text = "Waiting ...",
          background = 'blue', foreground ="white",
          font = ("Times New Roman", 15))
        self.recog_label.place(x=480, y=35)

        # Combobox creation
        self.n = tk.StringVar()
        self.monthchoosen = ttk.Combobox(self, width = 20, textvariable = self.n)

        # Adding combobox drop down list
        self.monthchoosen['values'] = (
                ' 0,INVALID',
                ' 1,NFC-A,15:99:F3:1E',
                ' 2,NFC-V,E0:02:23:00:56:BA:33:4E',
                ' 3,NFC-A,19:73:1D:00',
                ' 4,NFC-A,EB:02:7C:1F',
                ' 5,NFC-A,18:99:F3:1E',
                ' 6,NFC-A,19:99:F3:1E',
                ' 7,NFC-A,1A:99:F3:1E',
                ' 8,NFC-A,1B:99:F3:1E',
                ' 9,NFC-A,1C:99:F3:1E',
                ' 10,NFC-A,1D:99:F3:1E',
                ' 11,NFC-A,1E:99:F3:1E',
                ' 12,NFC-A,1F:99:F3:1E',
                ' 13,NFC-A,20:99:F3:1E',
                ' 14,NFC-A,21:99:F3:1E',
                ' 15,NFC-A,22:99:F3:1E')

        # self.monthchoosen.grid(column = 1, row = 5)
        self.monthchoosen.current(0)
        self.monthchoosen.place(x=12, y=33)

ext_list = ['gif', 'centerlight', 'glasses', 'happy', 'sad', 'leflight',
            'wink', 'noglasses', 'normal', 'sleepy', 'surprised', 'rightlight']
n_classes = 15
# Set up dataSet
dataSet = YaleFaceDataSet(constant.FACE_DATA_PATH, ext_list, n_classes)

mutex = Lock()
q = queue.Queue()

# board talk
path = sys.argv[1]
ser = serial.Serial(path, 115200, timeout = 1, rtscts=0)
print(ser.name)

root = Root()

rcv = None
rcv = Thread(target=receive, args=(ser, root))
rcv.start()

root.mainloop()
q.put("quit")
rcv.join()
print("Quit")

# Face detector
# face_detector = MTCnnDetector(constant.CELEBRITY_VGG_PATH)
# resized_faces = face_detector.process_image()


#exec_conv_model = True
#if exec_conv_model:
#    plt.imshow(dataSet.objects[0])
#    plt.show()
#else:
#    dataSet.get_data(vgg_img_processing=True)
#    vgg = VggModel(dataSet)
#    vgg.train(batch=20)
#    vgg.evaluate()
